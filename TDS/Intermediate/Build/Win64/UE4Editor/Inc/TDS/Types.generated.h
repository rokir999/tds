// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TDS_Types_generated_h
#error "Types.generated.h already included, missing '#pragma once' in Types.h"
#endif
#define TDS_Types_generated_h

#define TDS_Source_TDS_FuncLibrary_Types_h_19_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCharacterSpeed_Statics; \
	TDS_API static class UScriptStruct* StaticStruct();


template<> TDS_API UScriptStruct* StaticStruct<struct FCharacterSpeed>();

#define TDS_Source_TDS_FuncLibrary_Types_h_32_RPC_WRAPPERS
#define TDS_Source_TDS_FuncLibrary_Types_h_32_RPC_WRAPPERS_NO_PURE_DECLS
#define TDS_Source_TDS_FuncLibrary_Types_h_32_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTupes(); \
	friend struct Z_Construct_UClass_UTupes_Statics; \
public: \
	DECLARE_CLASS(UTupes, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TDS"), NO_API) \
	DECLARE_SERIALIZER(UTupes)


#define TDS_Source_TDS_FuncLibrary_Types_h_32_INCLASS \
private: \
	static void StaticRegisterNativesUTupes(); \
	friend struct Z_Construct_UClass_UTupes_Statics; \
public: \
	DECLARE_CLASS(UTupes, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TDS"), NO_API) \
	DECLARE_SERIALIZER(UTupes)


#define TDS_Source_TDS_FuncLibrary_Types_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTupes(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTupes) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTupes); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTupes); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTupes(UTupes&&); \
	NO_API UTupes(const UTupes&); \
public:


#define TDS_Source_TDS_FuncLibrary_Types_h_32_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTupes(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTupes(UTupes&&); \
	NO_API UTupes(const UTupes&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTupes); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTupes); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTupes)


#define TDS_Source_TDS_FuncLibrary_Types_h_32_PRIVATE_PROPERTY_OFFSET
#define TDS_Source_TDS_FuncLibrary_Types_h_29_PROLOG
#define TDS_Source_TDS_FuncLibrary_Types_h_32_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TDS_Source_TDS_FuncLibrary_Types_h_32_PRIVATE_PROPERTY_OFFSET \
	TDS_Source_TDS_FuncLibrary_Types_h_32_RPC_WRAPPERS \
	TDS_Source_TDS_FuncLibrary_Types_h_32_INCLASS \
	TDS_Source_TDS_FuncLibrary_Types_h_32_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TDS_Source_TDS_FuncLibrary_Types_h_32_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TDS_Source_TDS_FuncLibrary_Types_h_32_PRIVATE_PROPERTY_OFFSET \
	TDS_Source_TDS_FuncLibrary_Types_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	TDS_Source_TDS_FuncLibrary_Types_h_32_INCLASS_NO_PURE_DECLS \
	TDS_Source_TDS_FuncLibrary_Types_h_32_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TDS_API UClass* StaticClass<class UTupes>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TDS_Source_TDS_FuncLibrary_Types_h


#define FOREACH_ENUM_EMOVEMENTSTATE(op) \
	op(EMovementState::Aim_State) \
	op(EMovementState::Walk_State) \
	op(EMovementState::Run_State) 

enum class EMovementState : uint8;
template<> TDS_API UEnum* StaticEnum<EMovementState>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
